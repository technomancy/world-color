# World Color

For coloring SVG maps of the world interactively.

    $ racket world-color.rkt world.svg

Type a number (1-5) to categorize the selected country. Backspace is undo.

## Copying

Code distributed under the GNU General Public License version 3 or
later; see LICENSE.

[Robinson map image](https://commons.wikimedia.org/wiki/File:BlankMap-World_gray.svg)
courtesy of Wikipedia, distributed under [Creative Commons Attribution-Share Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.en).
